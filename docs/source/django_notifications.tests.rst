django\_notifications.tests package
===================================

Module contents
---------------

.. automodule:: django_notifications.tests
   :members:
   :undoc-members:
   :show-inheritance:

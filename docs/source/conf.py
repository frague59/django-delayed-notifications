"""Conf for django-notifications sphinx docs."""
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath("../.."))

# -- Project information -----------------------------------------------------

project = "django-notifications"
copyright = "2022, François GUÉRIN <fguerin@ville-tourcoing.fr>"  # noqa: A001
author = "François GUÉRIN <fguerin@ville-tourcoing.fr>"

# The full version, including alpha/beta/rc tags
version = "0.6"
release = "0.6.19"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.imgmath",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    "**/migrations*",
    "**/fixtures*",
    "**/locale*",
    "**/tests*",
]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_theme_path = [
    sphinx_rtd_theme.get_html_theme_path(),
]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

intersphinx_mapping = {
    "python": ("https://docs.python.org/", None),
    "django": (
        "http://docs.djangoproject.com/en/3.2/",
        "https://docs.djangoproject.com/en/3.2/_objects/",
    ),
}

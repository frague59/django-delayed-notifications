django\_notifications.management.commands package
=================================================

Submodules
----------

.. toctree::
   :maxdepth: 4

   django_notifications.management.commands.send_notifications

Module contents
---------------

.. automodule:: django_notifications.management.commands
   :members:
   :undoc-members:
   :show-inheritance:

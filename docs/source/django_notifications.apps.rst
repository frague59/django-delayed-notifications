django\_notifications.apps module
=================================

.. automodule:: django_notifications.apps
   :members:
   :undoc-members:
   :show-inheritance:

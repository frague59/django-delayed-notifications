django\_notifications.admin.forms module
========================================

.. automodule:: django_notifications.admin.forms
   :members:
   :undoc-members:
   :show-inheritance:

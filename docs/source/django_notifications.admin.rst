django\_notifications.admin package
===================================

Submodules
----------

.. toctree::
   :maxdepth: 4

   django_notifications.admin.forms
   django_notifications.admin.inlines

Module contents
---------------

.. automodule:: django_notifications.admin
   :members:
   :undoc-members:
   :show-inheritance:

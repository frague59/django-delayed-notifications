django\_notifications.management package
========================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_notifications.management.commands

Module contents
---------------

.. automodule:: django_notifications.management
   :members:
   :undoc-members:
   :show-inheritance:

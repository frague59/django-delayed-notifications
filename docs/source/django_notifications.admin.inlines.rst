django\_notifications.admin.inlines module
==========================================

.. automodule:: django_notifications.admin.inlines
   :members:
   :undoc-members:
   :show-inheritance:

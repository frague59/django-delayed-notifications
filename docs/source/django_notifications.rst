django\_notifications package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   django_notifications.admin
   django_notifications.management
   django_notifications.migrations
   django_notifications.tests

Submodules
----------

.. toctree::
   :maxdepth: 4

   django_notifications.apps
   django_notifications.models

Module contents
---------------

.. automodule:: django_notifications
   :members:
   :undoc-members:
   :show-inheritance:

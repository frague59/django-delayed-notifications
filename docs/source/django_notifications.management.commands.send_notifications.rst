django\_notifications.management.commands.send\_notifications module
====================================================================

.. automodule:: django_notifications.management.commands.send_notifications
   :members:
   :undoc-members:
   :show-inheritance:

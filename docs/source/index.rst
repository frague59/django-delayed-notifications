.. django-notifications documentation master file, created by
   sphinx-quickstart on Mon Jan 10 13:22:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-notifications's documentation!
================================================

Django-notifications provides a simple way to store and send notifications to users.

Notifications, are sent through email messages, and are stored in the database.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   django_notifications

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

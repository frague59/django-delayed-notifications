django\_notifications.migrations package
========================================

Submodules
----------

.. toctree::
   :maxdepth: 4

   django_notifications.migrations.0001_initial
   django_notifications.migrations.0002_notificationconfig

Module contents
---------------

.. automodule:: django_notifications.migrations
   :members:
   :undoc-members:
   :show-inheritance:

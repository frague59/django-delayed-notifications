django\_notifications.models module
===================================

.. automodule:: django_notifications.models
   :members:
   :undoc-members:
   :show-inheritance:

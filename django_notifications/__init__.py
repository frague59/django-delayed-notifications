"""
Application to manage notifications.

:copyright: (c) 2022 by François GUÉRIN
:license: mit, see LICENSE for more details.
:creationdate: 06/01/2022 11:31
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: django_notifications

"""
__version__ = VERSION = "0.6.19"

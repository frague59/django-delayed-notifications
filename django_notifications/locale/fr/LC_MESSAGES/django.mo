��    7      �  I   �      �  0   �  
   �     �     �       
   .  
   9  <   D  2   �     �     �     �     �     �     �  *        1     9     O     ^     q     �     �  )   �     �     �  5   �  :   2  
   m  	   x  C   �     �  (   �                    8     T     b  
   u     �     �     �     �     �     �  
   �     �     �     �  	   �     �     	  )   	  O  @	  5   �
     �
     �
  #   �
  (     
   1     <  A   H  A   �     �  	   �  4   �               .  9   I     �     �     �     �     �  3   �  (     ;   @     |  '   �  k   �  p   (     �     �  X   �       :   (     c     o     |      �     �     �     �     �     �       
         +     C     ^     o     �     �     �     �     �  *   �     %             3   2              7                           1      6   0   &           *   "                                      #      (   /      ,          +   )   $   	                 .   
             '                5   !      4   -                    A notification has been sent for {notification}. Attachment Attachments Blocking messages field Blocking notifications field Created at Created by DEBUG - Email notification sent for {notification} to {user} DEBUG - Send notification(s) to the connected user Dates Debug Debug notifications email Delay Delay in seconds. Delay notifications Delay the notifications, using a cron job. Delayed Delayed notifications Delayed seding Delayed sending at Delayed status Email address to use as sender Email recipients Email to send the notification copies to. Emails Enable debug notifications Field to check if the user wants to receive messages. Field to check if the user wants to receive notifications. From email HTML body If enabled, notification copies will be sent to the provided email. Immediate notifications No notification sent for {notification}. Not sent Notification Notification configuration Notification configurations Notifications One email per line Recipients Related object Send notifications Sent Sent at Sent notifications Sent status State from State to States Subject Text body Unsent notifications Users {attachment_file_name} for {notification} Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-13 11:19+0100
Last-Translator: François GUÉRIN <fguerin@ville-tourcoing.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
 Une notification a été envoyé pour {notification}. Pièce jointe Pièces jointes Nom du champs bloquant les messages Nom du champs bloquant les notifications Ajouté le Ajouté par DEBUG - La notification {notification} a été envoyée à {user} DEBUG - Envoyer la(es) notification(s) à l'utilisateur connecté Dates Débogage Adresse électronique de débogage des notifications Délais Délais en secondes. Retarder les notifications Retarder les notifications en utilisant une tâche cron . Envoi différé Notifications retardées Envoi retardé Envoi retardé le Statut de retardement Adresse électronique à utiliser comme expéditeur Adresses électroniques de destinataires Adresse électronique d'envoi des copies des notifications. Adresses électroniques Activer les débogage des notifications Champs à vérifier sur le profil utilisateur pour valider si l'utilisateur souhaite recevoir les messages. Champs à vérifier sur le profil utilisateur pour valider si l'utilisateur souhaite recevoir les notifications. Expéditeur Corps du message, en HTML Si activé, une copie des notifications sera envoyé à l'adresse électronique fournie. Envoi immédiat Aucune notification n'a été envoyé pour {notification}. Non envoyé Notification Configuration des notifications Configurations des notifications Notifications Une adresse par ligne Destinataires Objet associé Envoyer les notifications Envoyé Envoyé le Notifications envoyées Statut d'envoi du courriel État de départ État d'arrivée États Sujet Corps du message, en texte Notifications non-envoyées Utilisateurs {attachment_file_name} pour {notification} 